This is the data volume container for the [Atlassian
Stash](https://registry.hub.docker.com/u/atlassian/stash/) Docker image.
